= Example Asciidoc

:author: Christoph Stoettner
:email: christoph.stoettner@stoeps.de
:revnumber: 1.0
:revdate: 2018-08-23
:revremark: Initial Release
:encoding: utf-8
:lang: en
:icons: font
:toc:
:imagesdir: ../images
:imagesoutdir: ../images
:doctype: article
:numbered:

== Icons

NOTE: Are you sure?

icon:twitter[] Twitter https://twitter.com[@stoeps] +
icon:linux[] Linux Icon +
icon:windows[] Windows Icon

:icons: font

NOTE: Are you sure?

icon:twitter[] Twitter https://twitter.com[@stoeps] +
icon:linux[] Linux Icon +
icon:windows[] Windows Icon

== Source Code / Include Files

.A Python function
[source,python]
----
def function():
    var x = 10
    return x
----


:OS: Linux
ifeval::["{OS}" == "Linux"]
icon:linux[] rocks!
endif::[]

:OS: Windows
ifeval::["{OS}" == "Windows"]
icon:windows[] really?
endif::[]

:OS: Linux

== Plantuml


[plantuml]
----
title Network Plan
actor user [
  User
  ]

actor admin [
  Administrator
  ]

node http [
  HTTP Server
  connect.example.com
  ]
node dmgr [
  WebSphere Deployment Manager
  dmgr.example.com
]

database db2 [
  DB2 Database Server
  db2.example.com
  ]

user -down-> http:443/tcp
admin -right-> dmgr:9043/tcp
admin -down-> db2:50000/tcp
dmgr -up-> http
dmgr -down-> db2

center footer Connections 6.0 Deployment
----

== Source with callout

[source]
----
def function:
    x = 'secret' # <1>
    print(secret)
    return 0
----
<1> Hardcoded variable

== Tables


.Hostnames
[%header, cols=3]
|===
|Hostname
|IP
|Function

|www.example.com
|192.168.1.100
|Webserver

|dmgr.example.com
|192.168.2.100
|Application Server

|db.example.com
|192.168.2.101
|Database Server
